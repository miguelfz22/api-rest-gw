'use strict'

//Imports

const port = process.env.PORT || 3100;

const cors = require('cors');

const express = require('express');
const logger = require('morgan');
const fetch = require('node-fetch');
const mongojs = require('mongojs');

//Imports referentes al tema de seguridad

const https = require('https');
const fs = require('fs');
const { response } = require('express');

//JSON con la declaracion de las claves

const opciones = {
    key: fs.readFileSync('./cert/key.pem'),
    cert: fs.readFileSync('./cert/cert.pem'),
}

//Enlace a la BBDD

const URL_WS = "localhost:27017/SD";

const app = express();

//Declaraciones

var db = mongojs(URL_WS); //Enlazamos con la DB "SD"
var id = mongojs.ObjectID; //Función para convertir un id textual en un object id

//Variables para requerir tokens de seguridad en la respuesta

var allowCrossTokenHeader = (req,res,next) => {
    res.header("Access-Control-Allow-Headers", "*");
    return next();
};

var allowCrossTokenOrigin = (req,res,next) => {
    res.header("Access-Control-Allow-Origin","*");
    return next();
}

///Seguridad///

var authorization = (req,res,next) => { //Función de autorización básica
    if(req.headers.token === "password1234") { //Si coincide la contraseña con el token de la cabecera
        return next();                          //devolvemos la llamada a next()
    }                                           
    else{
        return next(new Error("CONTRASEÑA INCORRECTA!"));
    }
}

//Middlewares

app.use(logger('dev')); //probar con: tiny, short, dev, common, combined
app.use(express.urlencoded({extended: false})); // parse application/x-www-form-urlencoded
app.use(express.json()); //parse application/json

app.use(cors());
app.use(allowCrossTokenHeader);
app.use(allowCrossTokenOrigin);

function auth (req,res,next){
    if(!req.headers.authorization){
        res.status(403).json({
            result: 'KO',
            mensaje: "No se ha enviado el token tipo Bearer en la cabecera Authorization",
        });
        return next(new Error("Falta token de autorizacion"));
    }

    const token = req.headers.authorization.split(" ")[1];
    console.log(token)

    if(token==="MITOKEN123456789"){
        req.params.token = token;
        return next();
    }

    res.status(401).json({
        result: 'KO',
        mensaje: "Acceso no autorizado a este servicio"
    });
    return next(new Error("Acceso no autorizado"));
}

/*
//añadimos un trigger previo a las rutas para dar soporte a múltiples colecciones
app.param("coleccion", (req, res, next, coleccion)=>{
    console.log('param /api/:coleccion');
    console.log('colección: ', coleccion);
    req.collection = db.collection(coleccion);
    return next();
});

//routes
app.get('/api', (req, res, next) =>{
    console.log('GET /api');
    console.log(req.params);
    console.log(req.collection);

    db.getCollectionNames((err, colecciones)=>{
        if(err) return next(err);
        res.json(colecciones);
    }); 
});
*/


app.get('/api/:coleccion',(req,res,next)=>{
    const queColeccion = req.params.coleccion;

    fetch(URL_WS+"/"+queColeccion)
    .then(res=>res.json())
    .then(json=>{
        res.json({
            result: 'OK',
            coleccion: queColeccion,
            elemento: json.elemento,
        });
    });
});

app.get('/api/:coleccion/:id', (req, res, next)=>{
    const queId = req.params.id;
    const queColeccion = req.params.coleccion;
    const queURL = `${URL_WS}/${queColeccion}/${queId}`;

    fetch(queURL)
        .then(res => res.json())
        .then(json => {
            res.json({
                result: 'OK',
                coleccion: queColeccion,
                elemento: json.elemento,
            });
        });
});

app.post('/api/:coleccion', auth, (req, res, next)=>{
    const queColeccion = req.params.coleccion;
    const nuevo = req.body;
    const token = req.params.token;
    const queURL = `${URL_WS}/${queColeccion}`;

    fetch(queURL, {
        method: 'POST',
        body: JSON.stringify(nuevo),
        headers:{
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        }
    })
        .then(res=>res.json())
        .then(json=>{
            res.json({
                result: 'OK',
                coleccion: queColeccion,
                nuevoElemento: json.elemento
            })
        });
});

app.put('/api/:coleccion/:id',auth, (req,res,next)=>{
    const queId = req.params.id;
    const queColeccion = req.params.coleccion;
    const nuevo = req.body;
    const token = req.params.token;
    const queURL = `${URL_WS}/${queColeccion}/${queId}`;

    fetch(queURL, {
        method: 'PUT',
        body: JSON.stringify(nuevo),
        headers:{
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        }
    })
        .then(res=>res.json())
        .then(json=>{
            res.json({
                result: 'OK',
                coleccion: queColeccion,
                nuevoElemento: json.elemento
            })
        });

});

app.delete('/api/:coleccion/:id',auth, (req, res, next)=>{ 
    const queId = req.params.id;
    const queColeccion = req.params.coleccion;
    const token = req.params.token;
    const queURL = `${URL_WS}/${queColeccion}/${queId}`;

    fetch(queURL, {
        method: 'DELETE',
        headers:{
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        }
    })
        .then(res=>res.json())
        .then(json=>{
            res.json({
                result: 'OK',
                coleccion: queColeccion,
                nuevoElemento: json.elemento
            })
        });
});

//Iniciamos la aplicación de forma no segura
/*
app.listen(port,() => {
    console.log(`API REST ejecutándose en http://localhost:${port}/api/{colecciones}`);
});
*/

//Iniciamos la aplicación de forma segura

https.createServer(opciones, app).listen(port, () => {
    console.log(`API RESTful CRUD ejecutándose en https://localhost:${port}/api/{colecciones}`);
});
